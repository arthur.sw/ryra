// Load VexTab module.
vextab = VexTabDiv;


$(function() {


	VexTab = vextab.VexTab;
	Artist = vextab.Artist;
	Renderer = Vex.Flow.Renderer;

	Artist.DEBUG = true;
	VexTab.DEBUG = false;

	// Create VexFlow Renderer from canvas element with id #output
	renderer = new Renderer($('#output')[0], Renderer.Backends.CANVAS);

	// Initialize VexTab artist and parser.
	Artist.NOLOGO = true;
	artist = new Artist(10, 10, 600*2, {scale: 0.8});
	
	vextab = new VexTab(artist);
	

	function render() {
	try {
		vextab.reset();
		artist.reset();
		vextab.parse($("#vextab-code").val());
		artist.render(renderer);
		$("#error").text("");
	} catch (e) {
		console.log(e);
		$("#error").html(e.message.replace(/[\n]/g, '<br/>'));
	}
	}

	$("#vextab-code").keyup(_.throttle(render, 250));


	render();

	let rythms = [
		'C/4', 
		':8 C-C/4', 
		':16 C-C-C-C/4', 
		':8 C/4 :16 C-C/4 ^3^', 
		':16 C-C/4 :8 C/4 ^3^', 
		':16 C/4 :8 C/4 :16 C/4 ^3^', 
		':8d C/4 :16 C/4 ^2^', 
		':16 C/4 :8d C/4', 
		':4 ##', 
		':8 ## :8 C/4',
		':16 ## :16 C-C-C/4',
		':8 ## :8 C-C/4',
		':16 ## :16 C/4 :8 C/4',
		':16 ## :8 C/4 :16 C/4',
		':8d ## :16 C/4',
		':16 ## :8d C/4',
	]

	let prefix = `tabstave notation=true key=C time=4/4 tablature=false
notes `
	
	let choiceArtist = new Artist(10, 10, 200, {scale: 0.8});
	let choiceVextab = new VexTab(choiceArtist);

	let selectedRythms = []

	let generateRythms = ()=> {
		vextab.reset();
		artist.reset();
		if(selectedRythms.length == 0) {
			return
		}
		let generatedRythms = prefix
		for(let i=0 ; i<32 ; i++) {
			generatedRythms += selectedRythms[Math.floor(selectedRythms.length * Math.random())] + ' ';
			if(i%4 == 0 && i > 0) {
				generatedRythms += ' | '
			}
			if(i == 16) {
				generatedRythms += '\n\n' + prefix
			}
		}
		vextab.parse(generatedRythms);
		artist.render(renderer);
		$("#error").text("");
		$("#vextab-code").val(generatedRythms);
	}

	let generateChoices = ()=> {
		let choices = $('#choices')
		for(let rythm of rythms) {
			let canvas = $('<canvas>').addClass('off')
			choices.append(canvas)
			let choiceRenderer = new Renderer(canvas.get(0), Renderer.Backends.CANVAS);
			
			choiceVextab.reset();
			choiceArtist.reset();
			choiceVextab.parse(prefix + rythm);
			choiceArtist.render(choiceRenderer);		

			canvas.click( (event)=> {
				if(canvas.hasClass('off')) {
					canvas.removeClass('off')
					selectedRythms.push(rythm)
				} else {
					canvas.addClass('off')
					let index = selectedRythms.indexOf(rythm)
					if(index >= 0)  {
						selectedRythms.splice(index, 1)
					}
				}
				generateRythms();
			})
		}
	}


	generateChoices();

	$('#output').click((event)=> {
		generateRythms()
	})

});
